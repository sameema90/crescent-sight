function moondata2(lon,lat,squareGrid,userInput3,userInput4){

  //   const location_api_url = 'https://countriesnow.space/api/v0.1/countries/positions';
  //   var q_value_store = [];
  //   var q_value_loc = [];

  //let userInput3 = document.querySelector("#start");

  //console.log("---> User Input Date " + userInput3.value);

  //api_url_array = 'https://api.ipgeolocation.io/astronomy?apiKey=4054658c8a974da4b8a9324b76ee6f7f&lat='+lat+'&long='+lon+'&date='+ '2022-07-29';
  //api_url_array = 'https://api.ipgeolocation.io/astronomy?apiKey=17d520e41641430da5ee34394d96d2de&lat='+lat+'&long='+lon+'&date='+ '2022-07-29';

  //-----------------------------------------------------------------------
  //var times = SunCalc.getPosition(2022-07-29,5.8559, -81.3385);
  //date_input = '2022-07-30 '+ '19:54:00';
  console.log(userInput3.value);
  date_input = String(userInput3.value) + ' ' +String(userInput4.value);
  console.log(date_input);
  //date_input = 'userInput3.value'+'userInput4.value';
  //var sun_data = SunCalc.getPosition(userInput3.value,lat, lon);
  //var sun_data = SunCalc.getPosition(2022-07-29,lat, lon);
  //var times = SunCalc.getTimes(new Date(date_input), 31.83, 35.24);
  //console.log(times.sunrise)
  var sun_data = SunCalc.getPosition(new Date(date_input),lat,lon);
  //console.log(new Date());

  //var moon_data = SunCalc.getMoonPosition(userInput3.value,lat, lon);
  var moon_data = SunCalc.getMoonPosition(new Date(date_input),lat,lon);
  //console.log(moon_data);

  function degrees(radians) {
    return radians * (180 / Math.PI);
  }

  function calculatecos(deg){
    var rad = (Math.PI/180)*deg;
    return Math.cos(rad);
  }

  var sun_azi = degrees(sun_data.azimuth);
  var moon_azi = degrees(moon_data.azimuth);

  console.log('sun azimuth deg ='+ (sun_azi));
  console.log('sun altitude deg ='+ degrees(sun_data.altitude));

  console.log('moon azimuth deg ='+ (moon_azi));
  console.log('moon altitude deg ='+degrees(moon_data.altitude));
  console.log('moon dist km ='+ moon_data.distance);
  
  //----------------------------------------------------------------------

    //let DAZ= (degrees(sun_data.azimuth)) - (degrees(moon_data.azimuth));
    //let DAZ=  Math.abs((sun_azi) - (moon_azi));
    let DAZ= (sun_azi) - (moon_azi);
    console.log('DAZ=' + DAZ);//fine

    let ARCV = Math.abs((degrees(sun_data.altitude))-(degrees(moon_data.altitude)));
    console.log('ARCV=' + ARCV);//fine

    let cosARCV = calculatecos(ARCV);
    //console.log('cosARCV=' + cosARCV);//fine

    let cosDAZ = calculatecos(DAZ);
    //console.log('cosDAZ=' + cosDAZ);//fine

    let cosARCL=0;
    let ARCL=0;

    if (ARCV>=22 || DAZ>=22){
      cosARCL = cosARCV*cosDAZ;
      //console.log('cosARCL=' + cosARCL);
      ARCL = degrees(Math.acos(cosARCL))
      console.log('ARCL.1=' + ARCL);
    }else{
      ARCL = Math.sqrt(Math.pow(ARCV,2)+Math.pow(DAZ,2));
      console.log('ARCL.2=' + ARCL);
    }
    
    let p = degrees(Math.asin(6378.14/moon_data.distance))*60;//fine

    console.log('p='+p);

    let SD = 0.27245*p;//fine

    //let sinh =Math.sin(degrees(moon_data.altitude)*(Math.PI/180));

    //let sinp =Math.sin(p*(Math.PI/180));

    //let SD1= SD*(1+((Math.sin(data.moon_altitude))*(Math.sin(p))));
    let SD1= SD*(1+(Math.sin(degrees(moon_data.altitude))*Math.sin(p)));//fine
    //console.log('SD1='+SD1);//fine

    //let W= (SD1*(1-(cosARCL)))/Math.pow(60,2);//wrong value
    let W= (SD1*(1-(Math.cos(ARCL))))/3600;
    console.log('W='+W);

    let q = (ARCV - (11.8371 - (6.3226*W) + (0.7319*Math.pow(W,2)) - (0.1018*Math.pow(W,3))))/10;
    console.log('q='+q);


    var greenIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var redIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var blueIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var yellowIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var blackIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-black.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });
  
    var orangeIcon = new L.Icon({
      iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-orange.png',
      //shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowSize: [41, 41]
    });



    function style2(feature) {
        return {
            fillColor: feature,
            weight: 1,
            opacity: 0.1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.3
        };
    }

    if (q > 0.216){
      //var circle = L.marker([lat,lon],{icon: yellowIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
    }else if (q > -0.014){
      //var circle = L.marker([lat,lon],{icon: orangeIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('orange')}).addTo(this.map);
    }else if (q > -0.160){
      //var circle = L.marker([lat,lon],{icon: greenIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('green')}).addTo(this.map);
    }else if (q > -0.232){
      //var circle = L.marker([lat,lon],{icon: blueIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('blue')}).addTo(this.map);
    }else if (q > -0.293){
      //var circle = L.marker([lat,lon],{icon: redIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('red')}).addTo(this.map);
    } else if ( q <= -0.293){
      //var circle = L.marker([lat,lon],{icon: blackIcon}).addTo(map);
      L.geoJSON(squareGrid,{style:style2('black')}).addTo(this.map);
    }

  

  //   //function getColor(q) {
  // if (q > 0.216){
  //   var circle = L.marker([lat,lon],{icon: greenIcon}).addTo(map);
  //   L.geoJSON(squareGrid,{style:style2('green')}).addTo(this.map);
  //   //return '#00FF00';
  // // console.log('entered green '+q);
  // //message2.innerHTML = "Moon is easily visible in your location";

  // }else if (0.216 >= q > -0.014){
  //     var circle = L.marker([lat,lon],{icon: blueIcon}).addTo(map);
  //     L.geoJSON(squareGrid,{style:style2('blue')}).addTo(this.map);
  //     //return '#0000FF';
    
  //   //message2.innerHTML = "Moon is visible under perfect consitions in your location";
  // }else if (-0.014 >= q > -0.160){
  //     var circle = L.marker([lat,lon],{icon: yellowIcon}).addTo(map);
  //     L.geoJSON(squareGrid,{style:style2('yellow')}).addTo(this.map);
  //     //return '#FFBF00';
    
  //   //message2.innerHTML = "You may need to use optical aid to find the new crescent in your location";
  // }else if (-0.160 >= q > -0.232){
  //     var circle = L.marker([lat,lon],{icon: orangeIcon}).addTo(map);
  //     L.geoJSON(squareGrid,{style:style2('orange')}).addTo(this.map);
  //     //return '#FFA500';
    
  //   //message2.innerHTML = "You will need to use optical aid to find the new crescent in your location";
  // }else if (-0.232 >= q > -0.293){
  //     var circle = L.marker([lat,lon],{icon: redIcon}).addTo(map);
  //     L.geoJSON(squareGrid,{style:style2('red')}).addTo(this.map);
  //     //return '#FF0000';
    
  //   //message2.innerHTML = "You will not be able to see the crescent with a telescope in your location";
  // } else if (-0.293 >= q ){
  //     var circle = L.marker([lat,lon],{icon: blackIcon}).addTo(map);
  //     L.geoJSON(squareGrid,{style:style2('black')}).addTo(this.map);
  //     //return '#000000';
    
  //   //message2.innerHTML = "Moon is not visible in your location";
  // }else {
  //   message2.innerHTML = "q must be between -0.293 and 0.216 to show visibility criteria";
  // }
      // return q > 0.216 ? '#00FF00' :
      //        q > -0.014  ? '#0000FF' :
      //        q > -0.160  ? '#FFBF00' :
      //        q > -0.232  ? '#FFA500' :
      //        q > -0.293   ? '#FF0000' :
      //                        '#000000' ;
            //  d > 10   ? '#FED976' :
            //             '#FFEDA0';
    
      
    //}

    //var color_mark = getColor(q);
    //var circle = L.marker([loc_lat,loc_long],{icon: color_mark}).addTo(map);


    function style(feature) {
      return {
          fillColor: getColor(q),
          weight: 2,
          opacity: 1,
          color: 'white',
          dashArray: '3',
          fillOpacity: 0.7
      };

    }

}
